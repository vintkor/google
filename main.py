import json
import os
import re
from enum import Enum
from abc import ABC, abstractmethod
from pprint import pprint
import pymorphy2


class SourceKeys(Enum):
    ID = 'id'
    TEXT = 'text'


class BaseWordBuilder(ABC):

    @abstractmethod
    def get_words(self, string):
        ...


class WordAnalyzer(BaseWordBuilder):
    """
    Класс для разбиения текса на слова
    """

    def __init__(self):
        self._words = []
        self.text = ''
        self._morph = pymorphy2.MorphAnalyzer(lang='ru')

    def get_words(self, text):
        self.text = text.lower()
        self._tokenize()
        self._normalize()

        return self._words

    def _tokenize(self):
        self._words = re.findall(r'\w+', self.text)

    def _normalize(self):
        for index, word in enumerate(self._words):
            morph = self._morph.parse(word)[0]
            self._words[index] = morph.normal_form


class SourceBuilder:
    """
    Класс для построения сруктуры для индексации
    """

    def __init__(self, folder):
        self.folder = folder
        self._sources = []

    def load(self):
        path = os.path.join(os.getcwd(), self.folder)

        for dir_ in os.walk(path):
            files = dir_[2]
            for file_name in files:
                with open(os.path.join(path, file_name), 'r') as f:
                    self._sources.append({
                        SourceKeys.ID.value: file_name,
                        SourceKeys.TEXT.value: f.read(),
                    })

        return self._sources


class IndexBuilder:
    """
    Класс для построения индекса
    """

    def __init__(self):
        self.index = {}
        self._word_builder = WordAnalyzer()

    @staticmethod
    def _create_path(path):
        if not os.path.exists(path):
            os.makedirs(path)

    def clear_old_index_by_doc(self, doc_id: str):
        to_delete = []
        for k, i in self.index.items():
            is_exists = i.pop(doc_id, False)
            if is_exists and not len(i.keys()):
                to_delete.append(k)

        for i in to_delete:
            del self.index[i]

    def create_index(self, documents):
        for document in documents:
            text = document[SourceKeys.TEXT.value].lower()
            words = self._word_builder.get_words(text)
            self.clear_old_index_by_doc(document[SourceKeys.ID.value])

            for idx, word in enumerate(words):

                if word not in self.index.keys():
                    self.index[word] = {document[SourceKeys.ID.value]: [idx]}
                else:
                    if doc := self.index[word].get(document[SourceKeys.ID.value]):
                        doc.append(idx)
                    else:
                        self.index[word][document[SourceKeys.ID.value]] = [idx]

        self._save_index()

    def _save_index(self):
        folder_path = os.path.join(os.getcwd(), 'index')
        self._create_path(folder_path)
        with open(os.path.join(folder_path, 'full_index.json'), 'w', encoding='utf-8') as f:
            json.dump(self.index, f, indent=4)


class Google:
    """
    Класс для выполнения поискового запроса
    """

    def __init__(self, word_analyzer):
        self._words_length = 0
        self._words = []
        self._index = self._load_index()
        self._word_analyzer = word_analyzer()
        self._raw_result = []
        self._exact_matches = []

    @staticmethod
    def _load_index():
        path = os.path.join(os.getcwd(), 'index', 'full_index.json')
        with open(path, 'r', encoding='utf-8') as f:
            return json.load(f)

    def search(self, query_string):
        words = self._word_analyzer.get_words(query_string)
        self._words.extend(words)
        self._words_length = len(self._words)
        self._prepare_raw_result()
        self._exact_match_search()

        # TODO подготовка списока для неточных совпадений
        matches = [set(i.keys()) for index, i in enumerate(self._raw_result)]
        matches.sort(key=lambda x: len(x), reverse=True)
        print(matches)

        return self._exact_matches

    def _one_word_query(self, word):
        return self._index.get(word)

    def _prepare_raw_result(self):
        """
        Подготавливает сырой массив данных по запросу
        """

        for word in self._words:
            if result := self._one_word_query(word):
                self._raw_result.append(result)

    def _get_index_by_docs(self):
        index_by_docs = {}
        for one_word_result in self._raw_result:
            for doc_name, positions in one_word_result.items():
                if index_by_docs.get(doc_name):
                    index_by_docs[doc_name].extend([positions])
                else:
                    index_by_docs[doc_name] = [positions]

        return index_by_docs

    def _exact_match_search(self):
        index_by_docs = self._get_index_by_docs()
        first_positions_in_all_files = [i[0][0] for i in index_by_docs.values()]
        each_words_positions = [[j+i for j in range(self._words_length)] for i in first_positions_in_all_files]

        for doc_name, positions in index_by_docs.items():
            for ewp in each_words_positions:
                counter = 0
                for index, position in enumerate(positions):
                    if ewp[index] in position:
                        counter += 1

                if counter == self._words_length:
                    self._exact_matches.append({doc_name: ewp})


# TODO: сначала формируем миссив результатов по всем словам
#   затем ищем из этих результатов точное вхождение
#   затем сортируем результат сперва по точным вхождениям
#   после по нескольким фразам
#   после оставшиеся


def create_index():
    """
    Создание индекса. Вызывать 1 раз в начале.
    """

    source = SourceBuilder('raw_sources').load()
    builder = IndexBuilder()
    builder.create_index(source)
    pprint(builder.index)


def search():
    """
    Непосредственно поиск
    """

    google = Google(WordAnalyzer)
    res = google.search('товар должен иметь')
    print('-' * 80)
    pprint(f'Results: {res}')


if __name__ == '__main__':
    search()
